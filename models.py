import json

from django.contrib.gis.db import models as gis_models
from django.contrib.gis.db.models.functions import Transform
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone

from observation.model_utils import PlotBase, PlotObsBase, TreeMixin


# Add some fake classes to allow running the project
class AdminRegion:
    pass
class Owner:
    pass
class OwnerType:
    pass
class TreeGroup:
    pass


class Inventory(models.Model):
    name = models.CharField("Inventar", max_length=150)
    geom = gis_models.PolygonField(srid=2056, blank=True, null=True)

    class Meta:
        db_table = 'inventory'

    def __str__(self):
        return self.name

    @classmethod
    def get_from_plot(cls, plot):
        return Inventory.objects.get(geom__contains=plot.center)

    def get_plots(self, srid=None):
        plot_qs = Plot.objects.filter(
            center__within=self.geom
        ).annotate(
            plotgeom=Transform('center', srid),# plotgeomexact=Transform('point_exact', srid)
        )
        return plot_qs

    def layer_urls(self):
        return {
            'Forêt': reverse('inventory-foret', args=[self.pk]),
        }

    def as_geojson(self, srid=None):
        if (srid is not None and srid != self.geom.srid):
            self.geom.transform(srid)
        geojson = {
            "type": "Feature",
            "id": self.pk,
            "properties": {
                "id": self.pk,
                "name": self.name,
            },
            "geometry": json.loads(self.geom.json),
        }
        return geojson


class Municipality(models.Model):
    name = models.CharField(max_length=50, unique=True)
    bfs_nr = models.IntegerField(null=True)
    plz = models.CharField(max_length=4, blank=True)
    kanton = models.CharField(max_length=2)
    the_geom = gis_models.MultiPolygonField(srid=2056, blank=True, null=True)

    class Meta:
        db_table = 'municipality'

    def __str__(self):
        return self.name

    @classmethod
    def get_from_plot(cls, plot):
        try:
            return Municipality.objects.get(the_geom__contains=plot.center)
        except Municipality.DoesNotExist:
            return None


class Plot(PlotBase):
    EXPO_CHOICES = (
        ('_', "plat"),
        ('N', "Nord"),
        ('S', "Sud"),
        ('E', "Est"),
        ('W', "Ouest"),
    )
    nr = models.CharField("N° de placette", max_length=15)
    center = gis_models.PointField("Centre placette", srid=2056)
    point_exact = gis_models.PointField(srid=2056, blank=True, null=True)
    gps_precision = models.DecimalField("Précision GPS [m]", max_digits=8, decimal_places=2, blank=True, null=True)
    sealevel = models.PositiveSmallIntegerField("Altitude", blank=True, null=True)
    slope = models.SmallIntegerField("Pente", null=True, blank=True)
    exposition = models.CharField("Exposition", max_length=1, choices=EXPO_CHOICES, blank=True)
    remarks = models.TextField("Remarques", blank=True)
    # Data from LFI:
    nais_typ = models.CharField(max_length=4, blank=True)
    lfi_bez = models.CharField(max_length=4, blank=True)

    @property
    def the_geom(self):
        return self.center

    def get_observations(self):
        return self.plotobs_set.prefetch_related('treeobs_set').order_by('date_created')

    def as_geojson(self, dumped=True, srid=None, geom_field='center', for_mobile=False):
        geom = getattr(self, geom_field)
        if (srid is not None and srid != geom.srid):
            geom.transform(srid)
        urlname = 'mobile_plotobs_detail_json' if for_mobile else 'plotobs_detail_json'
        perimeter = Perimeter.from_point(geom)
        geojson = {
            "type": "Feature",
            "id": self.pk,
            "properties": {
                "id": self.pk,
                "nr": self.nr,
                "slope": self.slope,
                "sealevel": self.sealevel,
                "radius": self.radius,
                "exposition": self.exposition,
                # initial values for devel_stage and forest_form
                "devel_stage": [perimeter.devel_stage_id, ''] if perimeter else ['', ''],
                "forest_form": [perimeter.forest_form_id, ''] if perimeter else ['', ''],
                "obsURLs": {str(obs.date_created.year): reverse(urlname, args=[obs.pk]) for obs in self.plotobs_set.all()},
                "plotData": [
                    {"field": "nr", "label": "N°", "showEmpty": True},
                    {"field": "sealevel", "label": "Altit.", "unit": "m", "showEmpty": True},
                    {"field": "slope", "label": "Pente", "unit": "%", "showEmpty": True},
                    {"field": "radius", "label": "Rayon", "unit": "m", "showEmpty": True},
                ],
            },
            "geometry": json.loads(geom.json),
        }
        return geojson


class DevelStage(models.Model):
    code = models.CharField("Code", max_length=30, unique=True)
    description = models.CharField("Description", max_length=100)

    class Meta:
        db_table = 'lt_devel_stage'

    def __str__(self):
        return self.description


class ForestForm(models.Model):
    code = models.CharField("Code", max_length=20, unique=True)
    description = models.CharField("Description", max_length=100)

    class Meta:
        db_table = 'lt_forest_form'

    def __str__(self):
        return self.description


class PlotObs(PlotObsBase):
    plot = models.ForeignKey(Plot, verbose_name="Placette", on_delete=models.CASCADE)
    date_created = models.DateTimeField("Créé le")
    municipality = models.ForeignKey(Municipality, blank=True, null=True, on_delete=models.PROTECT, verbose_name="Commune")
    forest_edgef = models.DecimalField("Facteur de lisière", max_digits=2, decimal_places=1)
    accessible = models.BooleanField("Forêt accessible", default=True)
    accessible_notes = models.TextField("Notes d’accessibilité", blank=True)
    devel_stage = models.ForeignKey(
        DevelStage, on_delete=models.PROTECT, blank=True, null=True, verbose_name="Étape de développement"
    )
    forest_form = models.ForeignKey(
        ForestForm, on_delete=models.PROTECT, blank=True, null=True, verbose_name="Forme de forêt"
    )
    remarks = models.TextField("Remarques", blank=True)

    class Meta:
        db_table = 'plot_obs'
        constraints = [
            models.CheckConstraint(
                check=models.Q(forest_edgef__gte=0, forest_edgef__lte=1), name='forest_edgef_betw_0_1'
            ),
        ]

    @classmethod
    def get_from_mobile_data(cls, data, plot_id):
        """Return PlotObs instance from data received from mobile forms."""
        plot = get_object_or_404(Plot, pk=plot_id)
        inventory = Inventory.get_from_plot(plot)
        municipality = Municipality.get_from_plot(plot)
        try:
            plotobs = PlotObs.objects.get(plot=plot, date_created__year=data['year'])
            # It's a new sync with existing data, overwrite old data with the new one
            plotobs.delete()
        except PlotObs.DoesNotExist:
            pass
        return PlotObs(
            plot=plot, date_created=timezone.now(), municipality=municipality,
        )

    def visible_fields(self, exclude_empty=True):
        return [
            field.name
            for field in self._meta.fields
            if (field.name not in ('id', 'plot')
                and (not exclude_empty or getattr(self, field.name) not in (None, '')))
        ]

    def previous_obs(self):
        return self.plot.plotobs_set.order_by('date_created').last()

    def as_geojson(self, **kwargs):
        initial_dumped = kwargs.get('dumped', True)
        kwargs.update({'dumped': False})
        data = super().as_geojson(**kwargs)
        data['features'][0]['properties']['year'] = self.date_created.year
        if initial_dumped:
            data = json.dumps(data, cls=DjangoJSONEncoder)
        return data


class Vita(models.Model):
    code = models.CharField(max_length=1, unique=True)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_vita'

    def __str__(self):
        return "%s (%s)" % (self.description, self.code)


class TreeSpecies(models.Model):
    TARIFKLASS_CHOICES = (
        ('epicea', 'Épicéa (HAHBART1)'),
        ('sapin', 'Sapin (HAHBART2)'),
        ('hetre', 'Hêtre (HAHBART7)'),
        ('autres_feu', 'autres feuillus (HAHBART12)'),
        ('autres_res', 'autres résineux (HAHBART6)'),
    )
    code = models.CharField(max_length=7)
    description = models.CharField(max_length=100)
    order = models.PositiveSmallIntegerField(default=0)
    tarif_klass20 = models.CharField(max_length=15, choices=TARIFKLASS_CHOICES, blank=True)

    class Meta:
        db_table = 'lt_tree_spec'

    def __str__(self):
        return self.description

    @property
    def abbrev(self):
        return self.code

    @classmethod
    def active_species(cls):
        return TreeSpecies.objects.all().order_by('order')


class Tree(TreeMixin, models.Model):
    plot = models.ForeignKey(Plot, on_delete=models.CASCADE)
    nr = models.CharField("Numéro", max_length=15)
    spec = models.ForeignKey(TreeSpecies, blank=True, null=True, on_delete=models.PROTECT)
    azimuth = models.PositiveSmallIntegerField("Grade (400)")
    distance = models.PositiveSmallIntegerField("en décimètres [dm]")

    class Meta:
        db_table = 'tree'


class TreeObs(models.Model):
    obs = models.ForeignKey(PlotObs, on_delete=models.CASCADE)
    tree = models.ForeignKey(Tree, on_delete=models.CASCADE, verbose_name="Arbre")
    dbh = models.PositiveSmallIntegerField("Diamètre en cm")
    vita = models.ForeignKey(Vita, verbose_name="État de vie", on_delete=models.PROTECT)
    height = models.PositiveSmallIntegerField("Hauteur [m]", null=True, blank=True)
    marked = models.BooleanField("Arbre marqué", default=False)
    remarks = models.TextField("Remarques", blank=True)

    class Meta:
        db_table = 'tree_obs'
        constraints = [
            models.UniqueConstraint(fields=['obs', 'tree'], name='unique_tree_per_plot_obs')
        ]


class Perimeter(models.Model):
    owner = models.CharField(max_length=25)
    devel_stage = models.ForeignKey(DevelStage, on_delete=models.PROTECT, blank=True, null=True)
    forest_form = models.ForeignKey(ForestForm, on_delete=models.PROTECT, blank=True, null=True)
    poly = gis_models.MultiPolygonField(srid=2056)

    class Meta:
        db_table = 'perimeter'

    @classmethod
    def from_point(cls, pt):
        return cls.objects.filter(poly__contains=pt).first()
