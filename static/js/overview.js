const CENTER_INITIAL = [47.47, 7.65];
const ZOOM_INITIAL = 17;

function randomInt(max) {
  return Math.floor(Math.random() * max);
}

async function setupMap(mapOptions) {
    var map = L.map('map-div', mapOptions || {});
    L.tileLayer.swiss({layer: 'ch.swisstopo.pixelkarte-farbe', useCache: true}).addTo(map);
    map.setView(CENTER_INITIAL, ZOOM_INITIAL);
    var resp = await fetch('/overview_map/data');
    var data = await resp.json();
    var invLayer = L.geoJSON(data.inventories, {
        style: (feat) => {
            return {color: `#${randomInt(9)}${randomInt(9)}8${randomInt(9)}ff`};
        }}
    ).addTo(map);
    var plotLayer = await L.geoJSON(data.plots, {pointToLayer: (feat, latlng) => {
        return new L.CircleMarker(latlng, {
            radius: 6, 
            fillOpacity: 0.7,
            color: data.plots_outside.includes(feat.id) ? '#ffce00' : (feat.properties.num_obs > 0 ? '#0EA60E' : '#3388ff')
        });
    }}).addTo(map);
    map.fitBounds(plotLayer.getBounds());
}


document.addEventListener("DOMContentLoaded", (event) => {
    setupMap({crs: L.CRS.EPSG2056});
});

