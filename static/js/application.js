'use strict';
import { Map } from './modules/map.js';
import { Caliper } from './modules/caliper.js';
import { FormSet } from './modules/forms.js';
import { TreeForm } from './modules/treeobs.js';
import { MobileApp } from './modules/app.js';
import { modal } from './modules/utils.js';

/*
 * Map class
 */
class KSPMap extends Map {
    static CENTER_INITIAL = [47.47, 7.65];
    static ZOOM_INITIAL = 17;
    static ZOOM_PLOT = 27;

    setupLayers() {
        this.orthoLayer = L.tileLayer.swiss({layer: 'ch.swisstopo.swissimage'});
        this.grundLayer = L.tileLayer.swiss({layer: 'ch.swisstopo.pixelkarte-farbe', maxZoom: 29, useCache: true});
        this.baseLayers = [this.grundLayer];
    }
}

class KSPTreeForm extends TreeForm {
    setValuesFromVita() {
        const vita = document.getElementById('id_vita');
        if (!vita) return;
        const vitaCode = vita.options[vita.selectedIndex].text.substr(-2, 1);
        const heightLine = document.getElementById('tr_height');
        const heightInput = document.getElementById('id_height');
        if (vitaCode == 'm') {
            heightLine.removeAttribute('hidden');
            heightInput.setAttribute('required', true);
        } else {
            heightLine.setAttribute('hidden', true);
            heightInput.removeAttribute('required');
        }
        super.setValuesFromVita();
    }
}

class KSPFormSet extends FormSet {
    initHandlers() {
        super.initHandlers();
        document.getElementById('id_slope').addEventListener('change', (ev) => {
            this.syncSlopeAndRadius(ev.target.value);
        });
        // Show warning when forest edge factor is lower than 0.6
        document.getElementById('id_forest_edgef').addEventListener('input', (ev) => {
            if (parseFloat(ev.target.value) < 0.6) {
                document.getElementById('forest_edgef_warning').style.display = 'block';
            } else {
                document.getElementById('forest_edgef_warning').style.display = 'none';
            }
        });
        document.getElementById('id_accessible').addEventListener('change', (ev) => {
            this.setAccessibility();
        });
        document.getElementById('input-premature-end').addEventListener('click', (ev) => {
            const obs = this.app.currentObs();
            this.validateForm(this.getActive());
            if (!obs.readyForSubmit()) return;
            modal.confirm("Terminer la saisie de cette placette ?").then(resp => {
                if (resp != 'ok') return;
                this.hideAll();
                obs.finish();
                obs.showDetails();
            });
        });
    }

    initForms(obs) {
        super.initForms(obs);
        if (document.getElementById('id_forest_edgef').value == '') document.getElementById('id_forest_edgef').value = '1';
        // Slope value on the left synced with the slope input
        this.syncSlopeAndRadius(document.getElementById('id_slope').value);
        this.setAccessibility();
    }

    syncSlopeAndRadius(slopeVal) {
        document.getElementById('plot-slope').innerHTML = slopeVal;
        // Adapt radius to new slope
        var radius = 9.77;
        if (slopeVal > 20) {
            let rounded = parseInt(Math.round(slopeVal/5.0)*5);
            // slopeMapping is globally defined in index.html
            radius = slopeMapping[rounded];
        }
        document.getElementById('plot-radius').innerHTML = radius + 'm';
        // Adapt circle radius on the map
        this.app.map.vLayers.plotCircle.setRadius(radius);
        this.app.currentPlot.properties.radius = radius;
    }

    setAccessibility() {
        // If plot is not accessible, show an additional note field, hide the 'next' button
        // and show a Terminer button.
        let checked = document.getElementById('id_accessible').checked;
        if (checked) {
            document.getElementById('tr_accessible_notes').value = '';
            document.getElementById('tr_accessible_notes').setAttribute('hidden', true);
            document.getElementById('input-premature-end').setAttribute('hidden', true);
            document.querySelector('form#step1 button.weiter').removeAttribute('hidden');
        } else {
            document.getElementById('tr_accessible_notes').removeAttribute('hidden');
            document.getElementById('input-premature-end').removeAttribute('hidden');
            document.querySelector('form#step1 button.weiter').setAttribute('hidden', true);
        }
    }

    showStep(step) {
        super.showStep(step);
        if (step == this.FINAL_STEP) {
            // Handle exact coords warning
            const hasExactCoords = (
                document.getElementById('id_exact_long').value.length > 0 && document.getElementById('id_exact_lat').value.length > 0
            );
            document.getElementById('coords_missing_warning').style.display = hasExactCoords ? 'none' : 'block';
        }
    }
}

class KSPApp extends MobileApp {
    constructor() {
        super();
        this.mapClass = KSPMap;
        this.formSet = new KSPFormSet(this, KSPTreeForm);
        this.caliper = new Caliper(this.formSet);
    }

    initMap(mapOptions) {
        super.initMap({crs: L.CRS.EPSG2056});
    }
}

window.app = new KSPApp();

document.addEventListener("DOMContentLoaded", (event) => {
    window.app.init();
});
