from django.contrib import admin
from django.contrib.gis.db import models as gis_models

from observation.admin import MyOSMWidget
from . import models


@admin.register(models.Municipality)
class MunicipalityAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Perimeter)
class PerimeterAdmin(admin.ModelAdmin):
    list_display = ['owner', 'devel_stage', 'forest_form']


@admin.register(models.Inventory)
class InventoryAdmin(admin.ModelAdmin):
    formfield_overrides = {
        gis_models.PolygonField: {'widget': MyOSMWidget},
    }


@admin.register(models.Plot)
class PlotAdmin(admin.ModelAdmin):
    list_display = ['nr', 'sealevel', 'slope', 'exposition', 'center']
    formfield_overrides = {
        gis_models.PointField: {'widget': MyOSMWidget},
    }


@admin.register(models.PlotObs)
class PlotObsAdmin(admin.ModelAdmin):
    list_display = ['date_created']


@admin.register(models.TreeSpecies)
class TreeSpeciesAdmin(admin.ModelAdmin):
    list_display = ['description', 'code', 'order']


@admin.register(models.Tree)
class TreeAdmin(admin.ModelAdmin):
    list_display = ['nr', 'spec', 'azimuth', 'distance']


@admin.register(models.Vita)
class VitaAdmin(admin.ModelAdmin):
    list_display = ['code', 'description']


@admin.register(models.TreeObs)
class TreeObsAdmin(admin.ModelAdmin):
    list_display = ['dbh', 'remarks']
    raw_id_fields = ['obs', 'tree']


@admin.register(models.DevelStage)
class DevelStageAdmin(admin.ModelAdmin):
    list_display = ['code', 'description']


@admin.register(models.ForestForm)
class ForestFormAdmin(admin.ModelAdmin):
    list_display = ['code', 'description']
