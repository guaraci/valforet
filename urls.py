from django.urls import include, path

from mobile.views import main
from .import views

urlpatterns = [
    path('inventory/list/', views.InventoryListView.as_view(), name="inventory-list"),
    path('inventory/<int:pk>/foret/', views.InventoryForetLayerView.as_view(), name="inventory-foret"),
    path('overview_map/', views.OverviewMap.as_view(), name="overview-map"),
    path('overview_map/data/', views.OverviewMapData.as_view(), name="overview-map-data"),
    path('', include('mobile.urls')),
    path('', main, name='home'),
]
