import json

from django.contrib.gis.db.models import Union
from django.db.models import Count, Q
from django.views.generic import TemplateView, View
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse

from .models import Inventory, Perimeter, Plot


class InventoryListView(View):
    """This view returns a list of all accessible inventories."""
    def get(self, request, *args, **kwargs):
        current_invents = Inventory.objects.all()
        return JsonResponse([
            {'pk': inv.pk,  'name': inv.name, 'url': reverse('inventory-geojson', args=[inv.pk])}
            for inv in current_invents
        ], safe=False)


class InventoryForetLayerView(View):
    def get(self, request, *args, **kwargs):
        inv = get_object_or_404(Inventory, pk=kwargs['pk'])
        perims = Perimeter.objects.filter(poly__bboverlaps=inv.geom).select_related('devel_stage', 'forest_form')
        geojson = {"type": "FeatureCollection", "features": [], "mapStyle": {
            "color": "#ffa500",
            "weight": 1,
            "opacity": 0.65,
            "fillOpacity": 0.15,
        }}
        for item in perims:
            item.poly.transform(4326)
            geojson["features"].append({
                "type": "Feature",
                "id": item.pk,
                "properties": {
                    "id": item.pk,
                    "devel_stage": item.devel_stage.description if item.devel_stage else '',
                    "forest_form": item.forest_form.description,
                    "popupContent": f"Type: {item.forest_form.description}" + (
                        f"<br>Développement: {item.devel_stage.description}" if item.devel_stage else ''
                    ),
                },
                "geometry": json.loads(item.poly.json),
            })
        return JsonResponse(geojson)


class OverviewMap(TemplateView):
    template_name = 'overview.html'


class OverviewMapData(View):
    def get(self, request, *args, **kwargs):
        invents = Inventory.objects.all()
        mpoly = Inventory.objects.all().aggregate(Union('geom'))['geom__union']
        plots_outside = Plot.objects.exclude(center__within=mpoly)
        inv_geojson = {"type": "FeatureCollection", "features": [inv.as_geojson(srid=4326) for inv in invents]}
        plots_geojson = {
            "type": "FeatureCollection",
            "features": [
                as_geojson(plot, srid=4326) for plot in Plot.objects.all(
                    ).annotate(num_obs=Count('plotobs', filter=Q(plotobs__date_created__year__gte=2022)))
            ]
        }
        return JsonResponse({
            'inventories': inv_geojson,
            'plots': plots_geojson,
            'plots_outside': list(plots_outside.values_list('pk', flat=True)),
        })


def as_geojson(plot, srid=None):
    """A version of plot.as_geojson with less data but num_obs field."""
    if (srid is not None and srid != plot.center.srid):
        plot.center.transform(srid)
    geojson = {
        "type": "Feature",
        "id": plot.pk,
        "properties": {
            "id": plot.pk,
            "nr": plot.nr,
            "num_obs": plot.num_obs,
        },
        "geometry": json.loads(plot.center.json),
    }
    return geojson
