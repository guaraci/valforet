from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('valforet', '0008_develstage_forestform'),
    ]

    operations = [
        migrations.AddField(
            model_name='treeobs',
            name='marked',
            field=models.BooleanField(default=False, verbose_name='Arbre marqué'),
        ),
    ]
