import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('valforet', '0007_plot_exposition'),
    ]

    operations = [
        migrations.CreateModel(
            name='DevelStage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=30, unique=True, verbose_name='Code')),
                ('description', models.CharField(max_length=100, verbose_name='Description')),
            ],
            options={
                'db_table': 'lt_devel_stage',
            },
        ),
        migrations.CreateModel(
            name='ForestForm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=20, unique=True, verbose_name='Code')),
                ('description', models.CharField(max_length=100, verbose_name='Description')),
            ],
            options={
                'db_table': 'lt_forest_form',
            },
        ),
        migrations.CreateModel(
            name='Perimeter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('owner', models.CharField(max_length=25)),
                ('poly', django.contrib.gis.db.models.fields.MultiPolygonField(srid=2056)),
                ('devel_stage', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='valforet.develstage')),
                ('forest_form', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='valforet.forestform')),
            ],
            options={
                'db_table': 'perimeter',
            },
        ),
        migrations.AddField(
            model_name='plotobs',
            name='devel_stage',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='valforet.develstage', verbose_name="Étape de développement"),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='forest_form',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='valforet.forestform', verbose_name="Forme de forêt"),
        ),
    ]
