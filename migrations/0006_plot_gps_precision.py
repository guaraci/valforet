from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('valforet', '0005_plotobs_accessible'),
    ]

    operations = [
        migrations.AddField(
            model_name='plot',
            name='gps_precision',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=8, null=True, verbose_name='Précision GPS [m]'),
        ),
    ]
