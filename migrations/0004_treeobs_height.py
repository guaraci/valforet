from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('valforet', '0003_treespecies_order'),
    ]

    operations = [
        migrations.AddField(
            model_name='treeobs',
            name='height',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Hauteur [m]'),
        ),
    ]
