import django.contrib.gis.db.models.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('valforet', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='plot',
            name='point_exact',
            field=django.contrib.gis.db.models.fields.PointField(blank=True, null=True, srid=2056),
        ),
    ]
