import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion
import observation.model_utils


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Inventory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150, verbose_name='Inventar')),
                ('geom', django.contrib.gis.db.models.fields.PolygonField(blank=True, null=True, srid=2056)),
            ],
            options={
                'db_table': 'inventory',
            },
        ),
        migrations.CreateModel(
            name='Municipality',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True)),
                ('bfs_nr', models.IntegerField(null=True)),
                ('plz', models.CharField(blank=True, max_length=4)),
                ('kanton', models.CharField(max_length=2)),
                ('the_geom', django.contrib.gis.db.models.fields.MultiPolygonField(blank=True, null=True, srid=2056)),
            ],
            options={
                'db_table': 'municipality',
            },
        ),
        migrations.CreateModel(
            name='Plot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.CharField(max_length=15, verbose_name='N° de placette')),
                ('center', django.contrib.gis.db.models.fields.PointField(srid=2056, verbose_name='Centre placette')),
                ('sealevel', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Altitude')),
                ('slope', models.SmallIntegerField(blank=True, null=True, verbose_name='Pente')),
                ('remarks', models.TextField(blank=True, verbose_name='Remarques')),
            ],
            options={
                'db_table': 'plot',
            },
        ),
        migrations.CreateModel(
            name='PlotObs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(verbose_name="Créé le")),
                ('forest_edgef', models.DecimalField(decimal_places=1, max_digits=2, verbose_name='Facteur de lisière')),
                ('remarks', models.TextField(blank=True, verbose_name='Remarques')),
                ('municipality', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='valforet.municipality', verbose_name='Commune')),
                ('plot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='valforet.plot', verbose_name='Placette')),
            ],
            options={
                'db_table': 'plot_obs',
            },
        ),
        migrations.CreateModel(
            name='Tree',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.CharField(max_length=15, verbose_name='Numéro')),
                ('azimuth', models.PositiveSmallIntegerField(verbose_name='Grade (400)')),
                ('distance', models.PositiveSmallIntegerField(verbose_name='en décimètres [dm]')),
                ('plot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='valforet.plot')),
            ],
            options={
                'db_table': 'tree',
            },
        ),
        migrations.CreateModel(
            name='TreeSpecies',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=4)),
                ('description', models.CharField(max_length=100)),
                ('tarif_klass20', models.CharField(blank=True, choices=[('epicea', 'Épicéa (HAHBART1)'), ('sapin', 'Sapin (HAHBART2)'), ('hetre', 'Hêtre (HAHBART7)'), ('autres_feu', 'autres feuillus (HAHBART12)'), ('autres_res', 'autres résineux (HAHBART6)')], max_length=15)),
            ],
            options={
                'db_table': 'lt_tree_spec',
            },
        ),
        migrations.CreateModel(
            name='Vita',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1, unique=True)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_vita',
            },
        ),
        migrations.CreateModel(
            name='TreeObs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dbh', models.PositiveSmallIntegerField(verbose_name='Diamètre en cm')),
                ('remarks', models.TextField(blank=True, verbose_name='Remarques')),
                ('obs', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='valforet.plotobs')),
                ('tree', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='valforet.tree', verbose_name='Arbre')),
                ('vita', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='valforet.vita', verbose_name='État de vie')),
            ],
            options={
                'db_table': 'tree_obs',
            },
        ),
        migrations.AddField(
            model_name='tree',
            name='spec',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='valforet.treespecies'),
        ),
        migrations.AddConstraint(
            model_name='treeobs',
            constraint=models.UniqueConstraint(fields=('obs', 'tree'), name='unique_tree_per_plot_obs'),
        ),
        migrations.AddConstraint(
            model_name='plotobs',
            constraint=models.CheckConstraint(check=models.Q(('forest_edgef__gte', 0), ('forest_edgef__lte', 1)), name='forest_edgef_betw_0_1'),
        ),
    ]
