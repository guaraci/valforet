# Generated by Django 4.1.2 on 2023-02-09 18:09

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('valforet', '0012_plot_lfi_bez_plot_nais_typ'),
    ]

    operations = [
        migrations.AddField(
            model_name='plotobs',
            name='created',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Erstellt am'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='operator',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, verbose_name='Operator'),
        ),
    ]
