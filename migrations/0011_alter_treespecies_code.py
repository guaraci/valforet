from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('valforet', '0010_alter_treespecies_code'),
    ]

    operations = [
        migrations.AlterField(
            model_name='treespecies',
            name='code',
            field=models.CharField(max_length=7),
        ),
    ]
