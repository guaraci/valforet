from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('valforet', '0006_plot_gps_precision'),
    ]

    operations = [
        migrations.AddField(
            model_name='plot',
            name='exposition',
            field=models.CharField(blank=True, choices=[('_', 'plat'), ('N', 'Nord'), ('S', 'Sud'), ('E', 'Est'), ('W', 'Ouest')], max_length=1, verbose_name='Exposition'),
        ),
    ]
