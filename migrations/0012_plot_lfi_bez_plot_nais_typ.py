from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('valforet', '0011_alter_treespecies_code'),
    ]

    operations = [
        migrations.AddField(
            model_name='plot',
            name='lfi_bez',
            field=models.CharField(blank=True, max_length=4),
        ),
        migrations.AddField(
            model_name='plot',
            name='nais_typ',
            field=models.CharField(blank=True, max_length=4),
        ),
    ]
