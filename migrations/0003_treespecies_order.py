from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('valforet', '0002_plot_point_exact'),
    ]

    operations = [
        migrations.AddField(
            model_name='treespecies',
            name='order',
            field=models.PositiveSmallIntegerField(default=0),
        ),
    ]
