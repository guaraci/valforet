from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('valforet', '0004_treeobs_height'),
    ]

    operations = [
        migrations.AddField(
            model_name='plotobs',
            name='accessible',
            field=models.BooleanField(default=True, verbose_name='Forêt accessible'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='accessible_notes',
            field=models.TextField(blank=True, verbose_name='Notes d’accessibilité'),
        ),
    ]
