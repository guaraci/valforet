from decimal import Decimal

from django import forms
from django.contrib.gis.geos import Point

from mobile.forms import SpeciesSelect
from .models import Plot, PlotObs, TreeObs, TreeSpecies


class POFormStep1(forms.ModelForm):
    # Plot field, must come before forest_edgef computing uses the radius, which depends on the slope
    slope = forms.IntegerField(label="Pente", required=False)
    forest_edgef = forms.DecimalField(
        label="Facteur de lisière", min_value=0.5, max_value=1.0, max_digits=2, decimal_places=1
    )
    template_name = 'mobile/form_step1.html'

    help_content = {
        'forest_edgef': {
            'title': "Calcul distance pour facteur de lisière",
            'template': 'mobile/forest_edgef.html',
        }
    }

    class Meta:
        model = PlotObs
        fields = (
            'slope', 'accessible', 'accessible_notes', 'forest_edgef', 'devel_stage', 'forest_form'
        )

    def save(self, *args, **kwargs):
        obs = super().save(*args, **kwargs)
        if self.cleaned_data.get('slope'):
            obs.plot.slope = self.cleaned_data['slope']
            obs.plot.save()
        return obs


class PlotForm(forms.Form):
    exact_long = forms.FloatField(label="Longitude exacte", required=False)
    exact_lat = forms.FloatField(label="Latitude exacte", required=False)
    gps_precision = forms.DecimalField(widget=forms.HiddenInput, required=False)
    template_name = 'mobile/form_step_plot.html'

    def __init__(self, instance=None, **kwargs):
        self.instance = instance.plot if instance else None
        super().__init__(**kwargs)

    def clean(self):
        self.point_exact = None
        lon, lat = self.cleaned_data.get('exact_long'), self.cleaned_data.get('exact_lat')
        if lon and lat:
            self.point_exact = Point(lon, lat, srid=2056)
            del self.cleaned_data['exact_long']
            del self.cleaned_data['exact_lat']

    def save(self, *args, **kwargs):
        if self.point_exact:
            self.instance.point_exact = self.point_exact
            if self.cleaned_data['gps_precision']:
                self.instance.gps_precision = Decimal(self.cleaned_data['gps_precision'])
            self.instance.set_elevation_from_swisstopo(save=False)
            self.instance.save()
        return self.instance


class RemarkForm(forms.ModelForm):
    template_name = 'mobile/form_step_remarks.html'

    class Meta:
        model = PlotObs
        fields = ['remarks']


TREE_FIELDS = ('species', 'nr', 'distance', 'azimuth')

class TreeForm(forms.ModelForm):
    species = forms.ModelChoiceField(
        label="Essence",
        queryset=TreeSpecies.objects.all().order_by('description'),
        widget=SpeciesSelect
    )
    nr = forms.IntegerField()
    distance = forms.DecimalField(label="Distance", max_digits=6, decimal_places=2)
    azimuth = forms.IntegerField(label="Azimuth", min_value=0, max_value=400, widget=forms.NumberInput(attrs={'class': 'speechEnabled'}))
    dbh = forms.IntegerField(label="Diam.", min_value=12, max_value=200)
    perim = forms.IntegerField(label="Circonférence", max_value=900, required=False)

    class Meta:
        model = TreeObs
        fields = TREE_FIELDS + (
            'tree', 'vita', 'height', 'dbh', 'marked', 'remarks',
        )
        widgets = {
            'tree': forms.HiddenInput,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # azimuth unit depends on user choice
        self.fields['distance'].unit = 'dm'
        self.fields['dbh'].unit = 'cm'

    def other_fields(self):
        for field in self.visible_fields():
            if field.name not in TREE_FIELDS + ('perim', 'markiert'):
                yield field

    def add_error(self, field, error):
        try:
            vita_code = self.fields['vita'].clean(self.data['vita']).code
        except Exception:
            pass
        else:
            if field in ('dbh', 'azimuth') and vita_code in ('c', 'x', 'y'):
                # Ignore dbh/azimuth errors for cut/not considered trees
                self.cleaned_data[field] = self.data[field]
                return
        super().add_error(field, error)

    def save(self, **kwargs):
        # Save tree fields if changed
        tree_changed = False
        for fname in TREE_FIELDS:
            tree_field = 'spec' if fname == 'species' else fname
            if getattr(self.instance.tree, tree_field) != self.cleaned_data[fname]:
                setattr(self.instance.tree, tree_field, self.cleaned_data[fname])
                tree_changed = True
        if tree_changed:
            self.instance.tree.save()
        return super().save(**kwargs)

def get_step_forms(data=None, instance=None):
    return [
        {'label': 'Étape 1', 'form': POFormStep1(data=data, instance=instance, initial={'forest_edgef': '1'})},
        {'label': 'Étape 2 - Coordonnées', 'form': PlotForm(data=data, instance=instance)},
        {'label': 'Étape 3 - Remarques', 'form': RemarkForm(data=data, instance=instance)},
    ]


def get_tree_form(data=None):
    return TreeForm(data=data)
